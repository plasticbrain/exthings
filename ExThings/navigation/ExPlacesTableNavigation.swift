//
//  ExPlacesTableNavigation.swift
//  ExThings
//
//  Created by Stephen Downs on 2018-10-24.
//  Copyright © 2018 plasticbrain. All rights reserved.
//

// All navigation concerns for ExPlacesTableViewController.

import UIKit
import SnapNavigation

class ExPlacesTableSegue: SnapNavigationSegue {
    override var mediation: (UIViewController, UIViewController) -> () {
        get {
            
            return { source, destination in
                if let exPlaceVC = destination.content as? ExPlaceConsumer,
                    let selectedPlaceVC = source.content as? ExPlaceConsumer {
                    exPlaceVC.exPlace = selectedPlaceVC.exPlace
                }
            }
            
        }
        set {
        }
    }
}

extension ExPlacesTableViewController {
    // Navigation stub for an unwind segue.
    @IBAction func goHome(segue: UIStoryboardSegue) {
        // No further action required.
    }
}
