//
//  ExSpeciesDetailNavigation.swift
//  ExThings
//
//  Created by Stephen Downs on 2018-10-24.
//  Copyright © 2018 plasticbrain. All rights reserved.
//

// All navigation concerns for ExSpeciesDetailViewController.

import UIKit
import SnapNavigation

// MARK: - Routes

enum SpeciesDetailRoute: CaseIterable {
    
    // Conformance to CaseIterable.
    static var allCases: [SpeciesDetailRoute] {
        let defaultExSpecies = ExSpecies.defaultExSpecies
        return [
            .speciesDetail(exSpecies: defaultExSpecies)
        ]
    }
    
    case speciesDetail(exSpecies: ExSpecies)
}

// MARK: - Navigators

class ExSpeciesDetailNavigator: SnapRouteNavigator {
    
    var navigation: SnapNavigation
    
    // MARK: Initialization
    
    init(source: UIViewController) {
        navigation = SnapNavigation(source: source, destination: source)
    }
    
    // MARK: Navigation
    
    func navigation<Route: CaseIterable>(for route: Route) -> SnapNavigation? {
        guard let route = route as? SpeciesDetailRoute else { return nil }
        switch route {
        case .speciesDetail(let exSpecies):
            // Set destination.
            navigation.destination = .viewControllerFactory { navigation in
                let sourceVC: UIViewController
                switch navigation.source {
                case let .viewController(vc):
                    sourceVC = vc
                }
                return sourceVC.storyboard?.instantiateViewController(withIdentifier: "exSpeciesDetailViewController") ?? UIViewController()
            }
            // Set mediation.
            navigation.mediation = .method({ source, destination in
                if let speciesConsumer = destination.content as? ExSpeciesConsumer {
                    speciesConsumer.exSpecies = exSpecies
                }
            })
            // Set presentation method.
            navigation.presentation = .show
            // Return updated navigation.
            return navigation
        }
    }
}



// Mediate all storyboard segues originating from ExSpeciesDetailViewController.
class ExSpeciesDetailSegue: SnapNavigationSegue {
    override var mediation: (UIViewController, UIViewController) -> () {
        get {
            
            switch identifier {
                
            // Show map.
            case "showMap":
                return { source, destination in
                    if let destinationVC = destination.content as? AnnotatedPlaceConsumer,
                        let habitat = (source.content as? ExSpeciesDetailViewController)?.exSpecies?.habitat[0] {
                        destinationVC.annotatedPlace = habitat
                    }
                }
                
            // Show note editor.
            case "showNote":
                return { source, destination in
                    (destination.content as? TextConsumer)?.text = (source.content as? ExSpeciesDetailViewController)?.exSpecies?.notes
                }
                
            default:
                return {_, _ in }
                
            }
        }
        set {
            // Irrelevant.
        }
    }
}

// Other navigation concerns that must be handled internally.
extension ExSpeciesDetailViewController {
    
    // Embed segue for the ExSpecies widget.
    // Embed segues must be handled by the prepare(for:sender:) method.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
            
        case "embedExSpeciesWidget":
            if let similarSpeciesWidget = segue.destination as? ExSpeciesWidgetViewController {
                similarSpeciesWidget.dataSource = self
                similarSpeciesWidget.delegate = self
            }
            
        default:
            break
            
        }
    }
    
    // Unwind segue handler.
    @IBAction func selectSaveNote(segue: UIStoryboardSegue) {
        if let noteEditorVC = segue.source.content as? NoteEditorViewController,
            exSpecies != nil
        {
            exSpecies!.notes = noteEditorVC.text
            exSpeciesService.save(exSpecies!)
            updateViewFromData()
        }
    }
    
}
