//
//  ExThingsTableNavigation.swift
//  ExThings
//
//  Created by Stephen Downs on 2018-10-24.
//  Copyright © 2018 plasticbrain. All rights reserved.
//

import UIKit
import SnapNavigation

class ExThingsTableSegue: SnapNavigationSegue {
    override var mediation: (UIViewController, UIViewController) -> () {
        get {
            
            guard let exThingsTableVC = source.content as? ExThingsTableViewController,
                let selectedExThing = exThingsTableVC.selectedExThing else { return { _, _ in } }
            
            switch selectedExThing {
                
            // Show ExPlace detail view.
            case let selectedExPlace as ExPlace:
                (destination.content as? ExPlaceConsumer)?.exPlace = selectedExPlace
                return { _, _ in }
                
            // Show ExSpecies detail view.
            case let selectedExSpecies as ExSpecies:
                (destination.content as? ExSpeciesConsumer)?.exSpecies = selectedExSpecies
                return { _, _ in }
                
            default :
                return { _, _ in }
            }
        }
        set {
            // No need.
        }
    }
}

// Internal navigation concerns.
extension ExThingsTableViewController {
    // Navigation stub for an unwind segue.
    @IBAction func goHome(segue: UIStoryboardSegue) {
        // Nothing needs doing, but it's good to be home.
    }
}
