//
//  ExSpeciesTableNavigation.swift
//  ExThings
//
//  Created by Stephen Downs on 2018-10-24.
//  Copyright © 2018 plasticbrain. All rights reserved.
//

// All navigation concerns for ExSpeciesTableViewController.

import UIKit
import SnapNavigation

class ExSpeciesTableSegue: SnapNavigationSegue {
    override var mediation: (UIViewController, UIViewController) -> () {
        get {
            
            return { source, destination in
                if let exSpeciesConsumer = destination.content as? ExSpeciesConsumer,
                    let exSpeciesTableVC = source.content as? ExSpeciesTableViewController {
                   exSpeciesConsumer.exSpecies = exSpeciesTableVC.selectedExSpecies
                }
            }
            
        }
        set {
            // No need.
        }
    }
}

    // Navigation stub for an unwind segue.
extension ExSpeciesTableViewController {
    @IBAction func goHome(segue: UIStoryboardSegue) {
        // No further questions at this time your honor.
    }
}
