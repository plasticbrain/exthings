//
//  ExPlaceDetailNavigation.swift
//  ExThings
//
//  Created by Stephen Downs on 2018-10-23.
//  Copyright © 2018 plasticbrain. All rights reserved.
//

// All navigation concerns for ExPlaceDetailViewController.

import UIKit
import SnapNavigation

//  Handles navigation mediation concerns for all storyboard segues attached to ExPlaceDetailViewController.
class ExPlaceDetailSegue: SnapNavigationSegue {
    override var mediation: (UIViewController, UIViewController) -> () {
        get {
            switch identifier {
                
            // Show ExPlace detail view.
            case "showExPlaceDetail":
                return { source, destination in
                    (destination.content as? ExPlaceConsumer)?.exPlace = (source.content as? ExPlaceDetailViewController)?.nearestExPlace
                }
                
            // Show ExSpecies detail view.
            case "showExSpeciesDetail":
                return { source, destination in
                    (destination.content as? ExSpeciesConsumer)?.exSpecies = (source.content as? ExPlaceDetailViewController)?.nearestExSpecies
                }
                
            // Show map view.
            case "showMap":
                return { source, destination in
                    (destination.content as? AnnotatedPlaceConsumer)?.annotatedPlace = (source.content as? ExPlaceDetailViewController)?.exPlace
                }
                
            // Show note editor.
            case "showNote":
                return { source, destination in
                    (destination.content as? TextConsumer)?.text = (source.content as? ExPlaceDetailViewController)?.exPlace?.notes
                }
                
            default:
                return { _, _ in }
            }
            
        }
        set {
            // No need.
        }
    }
}

// Unwind segue action for selectSaveNote.
extension ExPlaceDetailViewController {
    @IBAction func selectSaveNote(segue: UIStoryboardSegue) {
        if let noteEditor = segue.source as? TextConsumer,
            exPlace != nil
        {
            exPlace!.notes = noteEditor.text
            exPlacesService.save(exPlace!)
        }
    }
}
